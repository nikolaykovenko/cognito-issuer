global.fetch = require('node-fetch');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');

const userPoolId = process.argv[2];
const clientId = process.argv[3];
const organisationUuid = process.argv[4];
const userRef = process.argv[5];
const userPassword = process.argv[6];

const userName = `irn:ic3:iam:eu-west-1:${organisationUuid}:driver:${userRef}`;

const userPool = new AmazonCognitoIdentity.CognitoUserPool({
    UserPoolId: userPoolId,
    ClientId: clientId
});

const cognitoUser = new AmazonCognitoIdentity.CognitoUser({
    Username: userName,
    Pool: userPool
});

const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
    Username: userPool,
    Password: userPassword,
});

cognitoUser.setAuthenticationFlowType('USER_PASSWORD_AUTH');

cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: function (result) {
        console.log(result.getIdToken());
    },

    onFailure: function(err) {
        console.log('error');
        console.log(err);
        // User authentication was not successful
    }
});
